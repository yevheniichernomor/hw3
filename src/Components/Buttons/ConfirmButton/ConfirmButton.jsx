import './ConfirmButton.scss'
import PropTypes from 'prop-types';

const ConfirmButton = (props) => {

    const { text, onClick } = props
    return (

        <>
            <button className='confirm-btn' onClick={onClick}>{text}</button>

        </>
    )

};
ConfirmButton.propTypes = {
    text: PropTypes.string,
    onClick: PropTypes.func,

}
export default ConfirmButton